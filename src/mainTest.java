import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Tshepo on 10/9/2015.
 */
public class mainTest {
    public static void main(String[] args) {

        String[] choice = {"paper", "rock", "scissor"};
        System.out.println("Type in 'E' to exit");
        JOptionPane.showMessageDialog(null, "*Welcome to Rock-Paper-Scissors*");
        String userInput = "";
        String computeChoice = "";

        int [] weight = {1,1,1};
        ArrayList record = new ArrayList();

        while (!userInput.equalsIgnoreCase("e")) {
            userInput = JOptionPane.showInputDialog(null, "Please enter your choice");
            computeChoice = generateWeapon(choice, weight, userInput, computeChoice);
            play(choice, userInput, weight, record, computeChoice);

        }

    }

    public static void play(String [] weapon, String input, int [] weaponWeight, ArrayList records, String computerChoice){

        if (!input.equalsIgnoreCase("paper") || !input.equalsIgnoreCase("rock") || !input.equalsIgnoreCase("scissor")) {
            JOptionPane.showMessageDialog(null, "Please type Rock, Paper, Scissors");

        }else {
            int totalCumulative = 0;

            for (int index = 0; index < weaponWeight.length; index++) {
                totalCumulative += weaponWeight[index];
            }


            if (input.equalsIgnoreCase("rock") && computerChoice.contains("scissors")) {
                weaponWeight[0] += 1;

            } else if (input.equalsIgnoreCase("rock") && computerChoice.contains("scissors")) {
                weaponWeight[1] += 1;
            } else if (input.equalsIgnoreCase("rock") && computerChoice.contains("scissors")) {
                weaponWeight[2] += 1;
            }


            if (input.equalsIgnoreCase("rock") && computerChoice.contains("scissors")) {

                System.out.println("Rock beats scissors");
                System.out.println("You won");
            }
            if (input.equalsIgnoreCase("scissors") && computerChoice.contains("rock")) {

                System.out.println("Rock beats scissors");
                System.out.println("The computer won");
            }
            if (input.equalsIgnoreCase("scissors") && computerChoice.contains("paper")) {
                System.out.println("Scissors beat paper");
                System.out.println("You won");
            }
            if (input.equalsIgnoreCase("paper") && computerChoice.contains("scissor")) {
                System.out.println("Scissors beat paper");
                System.out.println("You Lost");
            }
            if (input.equalsIgnoreCase("paper") && computerChoice.contains("rock")) {
                System.out.println("Paper beats rock");
                System.out.println("You won");
            }
            if (input.equalsIgnoreCase("rock") && computerChoice.contains("paper")) {
                System.out.println("Paper beats rock");
                System.out.println("You Lost");
            }
            if (input.equalsIgnoreCase("rock") && computerChoice.contains("rock")) {
                System.out.println("Nobody won");

            }
            if (input.equalsIgnoreCase("paper") && computerChoice.contains("paper")) {
                System.out.println("Nobody won");

            }
            if (input.equalsIgnoreCase("scissors") && computerChoice.contains("scissors")) {
                System.out.println("Nobody won");
            }
        }
    }

    public static String generateWeapon (String [] option, int [] weighted, String input, String computerChoice){
        ArrayList weapon = new ArrayList();
        int totalCumulative = 0;

        for(int addIndex = 0; addIndex < option.length; addIndex++){
            for(int checkIndex=0; checkIndex < option.length; checkIndex++){
                if (input.equalsIgnoreCase("paper")){
                    weighted[0] += 1;

                }else if (input.equalsIgnoreCase("rock")){
                    weighted[0] += 1;
                }else if (input.equalsIgnoreCase("scissor")){
                    weighted[0] += 1;
                }else {

                }

            }
            totalCumulative += weighted[addIndex];
            weighted[addIndex] = totalCumulative;
        }

        int rand = (int) Math.random() * totalCumulative;
        int count = 0;

        for (int determineWeapon = 0; determineWeapon < weighted.length; determineWeapon++) {

            if (weighted[determineWeapon] >= rand) {
                break;

            }else {
                count++;
            }
        }

        computerChoice = option[count];

        return computerChoice;
    }

}



