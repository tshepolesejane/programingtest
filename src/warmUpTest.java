
/**
 * Created by Tshepo on 10/9/2015.
 */
public class warmUpTest {
    public static void main (String [] args){
        double [] probabilities = {0.25, 0.5, 0.2, 0.05};
        String [] fruits = {"Apples","Pears","Grapes","Orange"};

        double totalCumulative = 0;

        for (int chance =  0; chance < probabilities.length; chance++){
            totalCumulative += probabilities[chance];
            probabilities[chance] = totalCumulative;
            System.out.print("The accumulated fruit\t" + fruits[chance] + "\t =>" + totalCumulative + "");
        }

        double rand = Math.random()* totalCumulative;
        System.out.println("\n"+rand);

       for(int checkIndex = 0; checkIndex < probabilities.length; checkIndex++ ) {

           if (probabilities[checkIndex] >= rand) {

               System.out.println("The randomly selected fruit "+ fruits[checkIndex]);
               break;

           }
       }

    }
}
